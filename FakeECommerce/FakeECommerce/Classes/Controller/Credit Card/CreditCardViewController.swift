//
//  CreditCardViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class CreditCardViewController: UIViewController {

    
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    
   
    
    
    @IBAction func actionBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func actionPay(_ sender: Any) {
        
        activityIndicatorView.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.performSegue(withIdentifier: "ThankYou", sender: nil)
            
            self.activityIndicatorView.stopAnimating()
        }
    }
    
    
    
}
