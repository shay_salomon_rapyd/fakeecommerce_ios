//
//  PaymentCategoriesViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class PaymentCategoriesViewController: UIViewController {

    
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    
    let images = ["payment-categories_pay-with-back-account-transfer",
                  "payment-categories_pay-with-online-banking",
                  "payment-categories_pay-with-cash",
                  "payment-categories_pay-with-eWallet",
                  "payment-categories_pay-with-credit-card"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    @IBOutlet weak var categoriesTableView: UITableView!
    
    
    
    @IBAction func actionBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = categoriesTableView.indexPathForSelectedRow {
            categoriesTableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    
    private func performSegueWithWait(_ name: String) {
        
        activityIndicatorView.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            self.performSegue(withIdentifier: name, sender: nil)
            
            self.activityIndicatorView.stopAnimating()
        }
    }
}




extension PaymentCategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return images.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentCategoriesTableViewCell
        
        cell.paymentImageView.image = UIImage(named: images[indexPath.row])!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0:
            
            performSegueWithWait("OfflineBank")
            
        case 1:
            
            performSegueWithWait("BankRedirect")
            
        case 2:
            
            performSegueWithWait("Cash")
            
        case 3:
            
            performSegueWithWait("QR")
            
        case 4:
            
            performSegueWithWait("CreditCard")
            
        default:
            break
        }
    }
}
