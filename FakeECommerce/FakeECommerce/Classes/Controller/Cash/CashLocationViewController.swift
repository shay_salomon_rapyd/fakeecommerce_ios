//
//  CashLocationViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class CashLocationViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    let images = ["cash_location1",
                  "cash_location2"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    
    @IBAction func actionBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let indexPath = tableView.indexPathForSelectedRow, indexPath.row == 1, let cashCodeViewController = segue.destination as? CashCodeViewController {
            
            cashCodeViewController.isIndomart = true
        }
    }
    
    
}



extension CashLocationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return images.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentCategoriesTableViewCell
        
        cell.paymentImageView.image = UIImage(named: images[indexPath.row])!
        
        return cell
    }
    
    
}
