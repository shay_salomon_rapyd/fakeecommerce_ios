//
//  CashCodeViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class CashCodeViewController: UIViewController {

    
    
    @IBOutlet weak var locationDetailsTitleImageView: UIImageView!
    @IBOutlet weak var codeImageView: UIImageView!
    @IBOutlet weak var instructionsImageView: UIImageView!
    
    
    var isIndomart = false
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isIndomart {
            
            locationDetailsTitleImageView.image = UIImage(named: "cash-code_indomart")
            codeImageView.image                 = UIImage(named: "cash-code_indomart_code")
            instructionsImageView.image         = UIImage(named: "cash-code_indomart_instructions")
        }
    }
    

   
    @IBAction func actionBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFinish(_ sender: Any) {
        
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
    
}
