//
//  OfflineBankProviderViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class OfflineBankProviderViewController: UIViewController {

    
    let images = ["offline-bank_bank1",
                  "offline-bank_bank2",
                  "offline-bank_bank3",
                  "offline-bank_bank4",
                  "offline-bank_bank5"]
    
    @IBOutlet weak var providerTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = providerTableView.indexPathForSelectedRow {
            providerTableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    

}



extension OfflineBankProviderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return images.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentCategoriesTableViewCell
        
        cell.paymentImageView.image = UIImage(named: images[indexPath.row])!
        
        return cell
    }        
}
