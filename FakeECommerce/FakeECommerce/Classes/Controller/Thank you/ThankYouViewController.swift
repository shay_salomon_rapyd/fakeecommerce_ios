//
//  ThankYouViewController.swift
//  FakeECommerce
//
//  Created by Shay Salomon on 17/03/2019.
//  Copyright © 2019 Rapyd. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func actionDone(_ sender: Any) {
        
        navigationController?.popToRootViewController(animated: true)
    }
    

}
